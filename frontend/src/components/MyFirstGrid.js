import React from 'react';
import {WebSocketContainerHB} from "./WebSocketContainerHB";
import {WebSocketContainerEvents} from "./WebSocketContainerEvents";
import {HeartBeat} from "./HeartBeat";
import GridLayout from 'react-grid-layout';

export class MyFirstGrid extends React.Component {
    render() {
        var layout = [
            {i: 'anime', x: 0, y: 0, w: 4, h: 10},
            {i: 'leftbar', x: 0, y: 4, w: 4, h: 20, static: true},
            {i: 'counter', x: 4, y: 0, w: 4, h: 8},
            {i: 'celeryevents', x: 4, y: 0, w: 12, h: 8},
            {i: 'celeryheartbeat', x: 4, y: 9, w: 12, h: 12}
        ];
        return (

            <GridLayout
                className="layout" layout={layout} cols={16} rowHeight={30} width={1800}
            >
                <div key="anime">
                    <HeartBeat/>
                </div>
                <div key="leftbar"/>
                {/*<div key="counter">*/}
                {/*    <WebSocketEvents*/}
                {/*        socketUrl={`${process.env.REACT_APP_WSS === 'true' ? 'wss': 'ws'}://${process.env.REACT_APP_BACKEND_HOSTNAME}/counter`}/>*/}
                {/*</div>*/}
                <div key="celeryevents">
                    <WebSocketContainerEvents
                        socketUrl={`${process.env.REACT_APP_WSS === 'true' ? 'wss' : 'ws'}://${process.env.REACT_APP_BACKEND_HOSTNAME}/celeryevents`}
                    />
                </div>
                <div key="celeryheartbeat">
                    <WebSocketContainerHB
                        socketUrl={`${process.env.REACT_APP_WSS === 'true' ? 'wss' : 'ws'}://${process.env.REACT_APP_BACKEND_HOSTNAME}/celeryheartbeat`}
                    />
                </div>
            </GridLayout>
        )
    }
}