import React, {useState} from 'react';
import {useSpring, animated} from 'react-spring';

export const HeartBeat = () => {
    const {o, xyz, color, strokeDasharray, strokeDashoffset, strokeOpacity, opacity} = useSpring({
        from: {
            o: 0,
            xyz: [0, 0, 0],
            color: 'green',
            strokeDashoffset: 0,
            strokeDasharray: 0,
            strokeOpacity: 0,
            opacity: 0
        },

        o: 1,
        xyz: [0, 640, 0],
        color: 'red',
        strokeDashoffset: 100,
        strokeDasharray: 1000,
        strokeOpacity: 100,
        opacity: 1
    });

    return (
        <svg width="640" height="640">
            <animated.path
                d="M126.06 48.03l-7.01 28.03-7 36.36-2.98 24.25-1.83 14.64-2.61 13.73-4.8 20.87c-1.17-2.13-1.9-3.47-2.19-4-2.37-4.33-3.61-9.2-3.61-14.14-1.3-10.2-2.11-16.57-2.44-19.12-.1-.78-1.17-.92-1.47-.19-.54 1.36-1.91 4.75-4.1 10.18l-4.01 10.6a49.126 49.126 0 01-26.21 7.58H41.97"
                id="a"
                fill-opacity="0" stroke="#000" strokeDasharray={10}
                strokeDashoffset={strokeDashoffset}/>
            <animated.path
                d="M205.89 179.63l-5.6-.8-6.8-.6-13-1.4h-1.37c-4.93 0-9.1-3.66-9.74-8.56-.01-.08-.04-.3-.09-.64l-1-7-2.4-10.2-2.4-13.4-2.4 6-3.6 14.2-1.4 7.2-1.6 8.4-.8 6-1.4 10.6-1.6 12-2.8 8.8-2.4 9.6-1.8 8.2-2 15.8-2.6-10.4-1.8-11.69-.8-10.2-1-18-.98-21.79c0-.02-.01-.09-.02-.21l-1.4-18.2-.4-14.6-1.6-23.8-1.4-22.6-1.6-13.8-1.4-14.8-.63-15.71"
                id="b" fill-opacity="0" stroke="#000" strokeDasharray={10}
                strokeDashoffset={strokeDashoffset}/>
        </svg>
    );
};