import asyncio
import logging
import uuid

import pytest
from async_asgi_testclient import TestClient
from celery import Celery
from celery.worker import WorkController

from app.main import fastapp
from app.tasks import celery1

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# logging.getLogger("aio_pika").setLevel(logging.DEBUG)


@pytest.mark.asyncio
async def test_homepage(asgiclient: TestClient) -> None:
    async with asgiclient:
        url: str = fastapp.url_path_for("homepage")
        resp = await asgiclient.get(url)
        assert resp.status_code == 200
        assert resp.json() == {"hello": "world"}


@pytest.mark.asyncio
async def test_version(asgiclient: TestClient) -> None:
    async with asgiclient:
        url = fastapp.url_path_for("version")
        resp = await asgiclient.get(url)
        assert resp.status_code == 200
        assert resp.json().get("fv") == "0.42.0"
        assert resp.json().get("sv") == "0.12.9"


def test_celery1(celery_app: Celery, celery_worker: WorkController) -> None:
    task = celery1.s("worD").apply()
    assert task.status == "SUCCESS"
    assert task.result == "celery1 task return worD"


@pytest.mark.asyncio
async def test_service(asgiclient: TestClient) -> None:
    async with asgiclient:
        async with asgiclient.websocket_connect("/celeryheartbeat") as session:
            r = await session.receive_json()
            assert r["type"] == "worker-heartbeat"


@pytest.mark.asyncio
async def test_service_events(asgiclient: TestClient) -> None:
    async with asgiclient:
        async with asgiclient.websocket_connect("/celeryevents") as session:
            url = fastapp.url_path_for("newtask")
            msg = str(uuid.uuid4())
            resp = await asgiclient.post(url, json={"msg": msg})
            assert resp.status_code == 200
            assert resp.json() == {"msg": f"Word {msg} received"}
            logger.debug("task service sent")
            j = await session.receive_json()
            await asyncio.sleep(0.1)
            assert "task-received" in j[0]["type"]
            j = await session.receive_json()
            await asyncio.sleep(0.1)
            assert "task-started" in j[0]["type"]
            j = await session.receive_json()
            await asyncio.sleep(0.1)
            assert "task-succeeded" in j[0]["type"]
