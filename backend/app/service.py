import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
if not logger.hasHandlers():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter: logging.Formatter = logging.Formatter(
        fmt="%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.propagate = False


# class CeleryLoggerAsyncService(object):
#     def __init__(self, broker_url: str, websocket: Optional[WebSocket] = None) -> None:
#         self._broker_url: str = broker_url
#         self.websocket: WebSocket = websocket
#         self.started: bool = False
#
#         self._consuming: bool = False
#         self._connection: Optional[aio_pika.Connection] = None
#         self._channel: Optional[aio_pika.Channel] = None
#         self._exchange: Optional[aio_pika.Exchange] = None
#         self._queue: Optional[aio_pika.Queue] = None
#
#     async def startup(self) -> None:
#         assert not self.started
#         self._connection = await aio_pika.connect(self._broker_url)
#         self._channel = await self._connection.channel()
#         await self._channel.set_qos(1)
#         self._exchange = await self._channel.declare_exchange(
#             name="task_log", type=ExchangeType.TOPIC, durable=True
#         )
#         self._queue = await self._channel.declare_queue(
#             name=f"task_log_queue", auto_delete=True
#         )
#         await self._queue.bind(exchange=self._exchange, routing_key="#.#")
#         self.started = True
#
#     async def shutdown(self) -> None:
#         logger.debug("shutting down loggers service")
#         if self.started:
#             await self._queue.unbind(self._exchange, routing_key="#")
#             await self._channel.close()  # type: ignore
#             await self._connection.close()  # type: ignore
#             logger.debug("loggers service shutted down")
#             self.started = False
#         else:
#             logger.debug("loggers service not started so no need to shutdown")
#
#     async def _process_message(self, message: aio_pika.IncomingMessage) -> None:
#         m: AsyncContextManager[aio_pika.IncomingMessage] = message.process()
#         async with m:
#             body: List[Dict[str, str]] = json.loads(message.body)
#             await self.websocket.send_json(body)
#
#     async def consume(self) -> None:
#         if not self._consuming:
#             assert self._queue
#             await self._queue.consume(callback=self._process_message)
#             self._consuming = True
#
