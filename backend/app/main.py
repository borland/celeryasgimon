import asyncio
import functools
import json
import logging
import typing
from logging import Logger
from typing import Dict, Union

import aio_pika
import fastapi
import starlette
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.background import BackgroundTasks
from starlette.datastructures import URLPath
from starlette.endpoints import WebSocketEndpoint
from starlette.requests import Request
from starlette.websockets import WebSocket

from app.celeryapp import celery_app
from app.models import (
    CeleryMsg,
    CeleryTaskFailed,
    CeleryTaskReceived,
    CeleryTaskRejected,
    CeleryTaskRetried,
    CeleryTaskRevoked,
    CeleryTaskSent,
    CeleryTaskStarted,
    CeleryTaskSucceeded,
    OneOfCeleryTask,
)

logger: Logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
if not logger.hasHandlers():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter: logging.Formatter = logging.Formatter(
        fmt="%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.propagate = False

fastapp = FastAPI()
# database = Database("postgresql://foo:bar@localhost:5344")


@fastapp.on_event("startup")
async def on_startup() -> None:
    logger.debug(f"on_event startup")
    logger.debug(f"on_event startup END")


@fastapp.on_event("shutdown")
async def on_shutdown() -> None:
    logger.debug(f"on_event shutdown")
    logger.debug(f"on_event shutdown END")


@fastapp.get("/")
async def homepage() -> Dict[str, str]:
    logger.debug(f"HOME")
    return {"hello": "world"}


@fastapp.get("/version")
async def version(request: Request) -> Dict[str, Union[str, URLPath]]:
    host_header = request.headers.get("host")
    endpoint = fastapp.url_path_for("homepage")
    url = fastapp.url_path_for("homepage").make_absolute_url(
        base_url=f"http://{host_header}"
    )
    fv = fastapi.__version__
    sv = starlette.__version__
    return {
        "host_header": host_header,
        "endpoint": endpoint,
        "url": url,
        "fv": fv,
        "sv": sv,
    }


@fastapp.post("/newtask")
def newtask(celerymsg: CeleryMsg) -> Dict[str, str]:
    logger.debug("sending new task")
    celery_app.send_task("tasks.celery1", args=[celerymsg.msg])
    logger.debug("new task sent")
    return {"msg": f"Word {celerymsg.msg} received"}


def write_notification(email: str, message: str = "") -> None:
    with open("/tmp/log.txt", mode="w") as email_file:
        content = f"notification for {email}: {message}"
        email_file.write(content)


@fastapp.post("/background/{email}")
async def send_notification(
    email: str, background_tasks: BackgroundTasks
) -> Dict[str, str]:
    background_tasks.add_task(write_notification, email, message="some notification")
    return {"message": "Notification sent in the background"}


class Item(BaseModel):
    amount: int


async def background_async(amount: int) -> None:
    logger.debug("sleeping")
    await asyncio.sleep(amount)
    logger.debug("slept")


@fastapp.post("/backgroundasync")
async def sleepingtheback(
    item: Item, background_tasks: BackgroundTasks
) -> Dict[str, str]:
    background_tasks.add_task(background_async, item.amount)
    return {"message": f"sleeping {item.amount} in the back"}


@fastapp.websocket_route("/celeryevents")
class CeleryEventsService(WebSocketEndpoint):
    async def on_connect(self, websocket: WebSocket) -> None:
        self._consuming: bool = False
        self._connection: aio_pika.Connection = await aio_pika.connect(
            celery_app.conf.broker_url
        )
        self._channel: aio_pika.Channel = await self._connection.channel()
        await self._channel.set_qos(1)
        self._exchange: aio_pika.Exchange = await self._channel.declare_exchange(
            name=celery_app.conf.event_exchange,
            type=aio_pika.ExchangeType.TOPIC,
            durable=True,
        )
        self._queue: aio_pika.Queue = await self._channel.declare_queue(
            name=f"{celery_app.conf.event_queue_prefix}.my_events_queue",
            auto_delete=True,
            arguments={
                "x-expires": int(celery_app.conf.event_queue_expires * 1000),
                "x-message-ttl": int(celery_app.conf.event_queue_ttl * 1000),
            },
        )
        await self._queue.bind(exchange=self._exchange, routing_key="#")
        await websocket.accept()
        self.cancel_event = asyncio.Event()
        self.consumer_task = asyncio.create_task(self.send_events(websocket))

    async def on_disconnect(self, websocket: WebSocket, close_code: int) -> None:
        self.consumer_task.cancel()
        logger.debug("disconnected")

    async def send_events(self, websocket: WebSocket) -> None:
        logger.debug("service send_events IN")
        async with self._queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    if message.routing_key == "task.multi":
                        logger.debug(
                            f"INCOMING message, exchange:{message.exchange}, routing_key:{message.routing_key}"
                        )
                        body: typing.List[Dict[str, str]] = json.loads(message.body)
                        await websocket.send_json(body)
                        handlers: Dict[str, typing.Type[OneOfCeleryTask]] = {
                            "task-succeeded": CeleryTaskSucceeded,
                            "task-sent": CeleryTaskSent,
                            "task-received": CeleryTaskReceived,
                            "task-started": CeleryTaskStarted,
                            "task-failed": CeleryTaskFailed,
                            "task-rejected": CeleryTaskRejected,
                            "task-revoked": CeleryTaskRevoked,
                            "task-retried": CeleryTaskRetried,
                        }
                        func = self.task_handler
                        event_type: typing.Optional[str] = body[0].get("type")
                        assert event_type is not None
                        model: typing.Optional[
                            typing.Type[OneOfCeleryTask]
                        ] = handlers.get(event_type)
                        dispatched = functools.partial(func, body, model)
                        result = await dispatched()
                        logger.debug(result)
                        # return result
                logger.debug("async with message.process()")
            logger.debug("async for message in queue_iter")
        logger.debug("async with self._queue.iterator() as queue_iter")

    async def task_handler(
        self, events: typing.List[Dict[str, str]], model: typing.Type[OneOfCeleryTask]
    ) -> OneOfCeleryTask:
        for event in events:
            m = model(**event)
            logger.debug(event)
            logger.debug(m)
            logger.debug(m.type.value)
            return m
        assert False  # ? mypy


@fastapp.websocket_route("/celeryheartbeat")
class CeleryHeartbeatService(WebSocketEndpoint):
    async def on_connect(self, websocket: WebSocket) -> None:
        self._consuming: bool = False
        self._connection: aio_pika.Connection = await aio_pika.connect(
            celery_app.conf.broker_url
        )
        self._channel: aio_pika.Channel = await self._connection.channel()
        await self._channel.set_qos(1)
        self._exchange: aio_pika.Exchange = await self._channel.declare_exchange(
            name=celery_app.conf.event_exchange,
            type=aio_pika.ExchangeType.TOPIC,
            durable=True,
        )
        self._queue: aio_pika.Queue = await self._channel.declare_queue(
            name=f"{celery_app.conf.event_queue_prefix}.my_heartbeat_queue",
            auto_delete=True,
            arguments={
                "x-expires": int(celery_app.conf.event_queue_expires * 1000),
                "x-message-ttl": int(celery_app.conf.event_queue_ttl * 1000),
            },
        )
        await self._queue.bind(exchange=self._exchange, routing_key="#")
        await websocket.accept()
        self.cancel_event = asyncio.Event()
        self.consumer_task = asyncio.create_task(self.send_heartbeat(websocket))

    async def on_disconnect(self, websocket: WebSocket, close_code: int) -> None:
        self.consumer_task.cancel()
        logger.debug("disconnected")

    async def send_heartbeat(self, websocket: WebSocket) -> None:
        logger.debug("service send_heartbeat IN")
        async with self._queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    logger.debug(f"body:{message.body}")
                    body: typing.List[Dict[str, str]] = json.loads(message.body)
                    await websocket.send_json(body)
                logger.debug("async with message.process()")
            logger.debug("async for message in queue_iter")
        logger.debug("async with self._queue.iterator() as queue_iter")


@fastapp.websocket_route("/counter")
class WebSocketTicks(WebSocketEndpoint):
    async def on_connect(self, websocket: WebSocket) -> None:
        await websocket.accept()
        self.ticker_task = asyncio.create_task(self.tick(websocket))
        logger.debug("connected")

    async def on_disconnect(self, websocket: WebSocket, close_code: int) -> None:
        self.ticker_task.cancel()
        logger.debug("disconnected")

    async def on_receive(self, websocket: WebSocket, data: typing.Any) -> None:
        await websocket.send_json({"Message: ": data})

    async def tick(self, websocket: WebSocket) -> None:
        counter = 0
        while True:
            logger.debug(counter)
            await websocket.send_json({"counter": counter})
            counter += 1
            await asyncio.sleep(5)


@fastapp.post("/longtask")
async def longtask_create() -> Dict[str, str]:
    celery_app.send_task("tasks.longtask")
    logger.debug("long task sent")
    return {"msg": f"longtask sent"}


def main() -> None:
    # uvicorn.run("main:fastapp", host="0.0.0.0", port=8000)
    uvicorn.run("main:fastapp", host="0.0.0.0", port=8000, reload=True)


if __name__ == "__main__":
    main()
