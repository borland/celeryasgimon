from functools import lru_cache

from pydantic import BaseSettings


class BrokerSettings(BaseSettings):
    rabbitmq_default_user: str
    rabbitmq_default_pass: str
    rabbitmq_hostname: str
    rabbitmq_port: str

    class Config:
        env_prefix = ""


@lru_cache()
def get_broker_settings() -> BrokerSettings:
    return BrokerSettings()  # reads variables from environment
