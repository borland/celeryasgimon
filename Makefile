#!make
-include .env_test_local
-include .env_test_dockerlocal

#export $(shell sed 's/=.*//' .env_test_local)

app_root = backend
app_root_ci =

pkg_src =  $(app_root)/app
pkg_src_ci =  $(app_root_ci)/app

tests_src = $(pkg_src)/tests
tests_src_ci = $(pkg_src_ci)/tests

isort = isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 88 --recursive --apply $(pkg_src) $(tests_src)
isort_ci = isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 88 --recursive --apply $(pkg_src_ci) $(tests_src_ci)

black = black $(pkg_src) $(tests_src)
black_ci = black $(pkg_src_ci) $(tests_src_ci)

flake8 = flake8 --config=$(app_root)/.flake8 $(pkg_src) $(tesbuildts_src)
flake8_ci = flake8 --config=$(pkg_src_ci)/.flake8 $(pkg_src_ci_ci) $(tests_src_ci)

mypy = mypy --config-file $(app_root)/mypy.ini $(pkg_src)
mypy_ci = mypy --config-file $(pkg_src_ci)/mypy.ini $(pkg_src_ci)


.PHONY: build
build:
	export $(shell sed 's/=.*//' .env_test_dockerlocal)
	sudo find . -type d -name __pycache__ -exec rm -r {} \+
	docker-compose -f docker-compose.yml build
	docker-compose -f docker-compose.yml down -v --remove-orphans
	docker-compose -f docker-compose.yml up -d

.PHONY: build-ci
build-ci:
	docker-compose -f docker-compose-build.yml build
	docker-compose -f docker-compose-build.yml down -v --remove-orphans
	docker-compose -f docker-compose-build.yml up -d

.PHONY: format
format:
	$(isort)
	$(black)

.PHONY: check-format
check-format:
	$(isort) --check-only
	$(black) --check

.PHONY: check-format-ci
check-format-ci:
	docker-compose -f docker-compose.yml exec -T backend-tests $(isort_ci) --check-only --verbose
	docker-compose -f docker-compose.yml exec -T backend-tests $(black_ci) --check

.PHONY: lint
lint:
	$(flake8)

.PHONY: lint-ci
lint-ci:
	docker-compose -f docker-compose.yml exec -T backend-tests $(flake8_ci)

.PHONY: mypy
mypy:
	$(mypy)

.PHONY: mypy-ci
mypy-ci:
	docker-compose -f docker-compose.yml exec -T backend-tests $(mypy_ci)

.PHONY: test
test:
	PYTHONPATH=$(app_root) pytest -sv $(tests_src) --log-cli-level=debug --cov=$(pkg_src) --cov-report html

.PHONY: testdockerlocal
testdockerlocal:
	docker-compose -f docker-compose.yml exec backend-tests sh -c '/wait-for.sh rabbitmq:5672 -t 30 -- pytest -sv tests --log-cli-level=debug --cov=. --cov-report html'

.PHONY: test-ci
test-ci:
	docker-compose -f docker-compose.yml exec -T backend-tests sh -c '/wait-for.sh rabbitmq:5672 -t 30 -- pytest -sv tests --log-cli-level=debug --cov=. --cov-report html'

.PHONY: clean
clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]' `
	rm -f `find . -type f -name '*~' `
	rm -f `find . -type f -name '.*~' `
	rm -rf `find . -type d -name '*.egg-info' `
	rm -rf `find . -type d -name 'pip-wheel-metadata' `
	rm -rf .cache
	rm -rf .pytest_cache
	rm -rf .mypy_cache
	rm -rf htmlcov
	rm -rf *.egg-info
	rm -f .coverage
	rm -f .coverage.*
	rm -rf build